import Detect from 'mobile-detect'

export const isAndroid = () => {
  const detect = new Detect(window.navigator.userAgent)
  return detect.is('AndroidOS')
}
