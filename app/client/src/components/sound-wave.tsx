import React, { useEffect, useRef } from 'react'
import { useSelector } from 'react-redux'

import './sound-wave.css'
import { VoltageLevel } from 'types/sound'
import { levelSelector } from 'slices/voltage'
import { randRange } from 'utils/math'

const MIN_BAR_HEIGHT = 10  // %

type Props = {
}
const SoundWave = React.memo((props: Props) => {
  return <>
    <div className="sound-wave">
      {[...Array(20)].map((_, index) => (
        <Bar key={index} />
      ))}
    </div>
  </>
})

type BarProps = {
}
const Bar = React.memo((props: BarProps) => {
  const level = useSelector(levelSelector)

  const ref = useRef<HTMLDivElement>(null)
  const animation = useRef<Animation>()

  useEffect(() => {
    if (level) {
      if (level === 'zero') {
        animation.current?.cancel()
      } else {
        const height = heightByLevel(level).map(n => `${n}%`)
        const duration =  durationByLevel(level)
        const delay = randRange(-1000, 0)

        animation.current?.cancel()
        animation.current = ref.current?.animate({
          height
        }, {
          duration,
          delay,
          direction: 'alternate',
          easing: 'ease-in-out',
          iterations: Infinity
        })
      }
    }
  }, [level])

  return <>
    <div ref={ref} className="bar" />
  </>
})

const heightByLevel = (level: VoltageLevel): number[] => {
  switch (level) {
    case 'high': {
      const min = MIN_BAR_HEIGHT + 20
      const max = randRange(min, 100)
      return [min, max]
    }
    case 'mid': {
      const min = MIN_BAR_HEIGHT + 10
      const max = randRange(min, 70)
      return [min, max]
    }
    case 'low': {
      const min = MIN_BAR_HEIGHT
      const max = randRange(min, 40)
      return [min, max]
    }
    default: {
      return [MIN_BAR_HEIGHT, MIN_BAR_HEIGHT]
    }
  }
}

const durationByLevel = (level: VoltageLevel): number => {
  switch (level) {
  case 'high':
    return randRange(200, 500)
  case 'mid':
    return randRange(500, 800)
  case 'low':
    return randRange(800, 1000)
  default:
    return 0
  }
}

export default SoundWave
