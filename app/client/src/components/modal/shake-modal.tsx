import { useCallback } from 'react'
import { useDispatch } from 'react-redux'
import Modal from 'react-modal'

import { AppDispatch } from 'app/store'
import { shakeImage } from 'assets/image'
import { setHasPermissionRequested } from 'slices/app'

const modalStyle: Modal.Styles = {
  content: {
    border: 0,
    bottom: 'auto',
    left: '50%',
    padding: '.75rem 1rem',
    position: 'fixed',
    right: 'auto',
    top: '50%',
    transform: 'translate(-50%,-50%)',
    width: '80%'
  },
  overlay: {
    transition: 'opacity 200ms ease-in-out',
    zIndex: 100
  }
}

type Props = {
  isActive: boolean,
  onCloseRequested: () => void
}
const ShakeModal = (props: Props) => {
  const dispatch: AppDispatch = useDispatch()
  const { isActive, onCloseRequested } = props

  const onButtonPressed = useCallback(() => {
    dispatch(setHasPermissionRequested({ hasPermissionRequested: true }))

    const MotionEvent = DeviceMotionEvent as any
    if (MotionEvent && MotionEvent.requestPermission) {
      MotionEvent.requestPermission()
        .then((state: string) => {
          console.log('[requestPermission] state', state)
        })
        .catch((err: Error) => {
          console.error('[requestPermission] error', err)
        })
    } else {
      console.error('[requestPermission] error', 'not supported')
    }

    onCloseRequested()
  }, [])

  return <>
    <Modal
      style={modalStyle}
      isOpen={isActive}
      closeTimeoutMS={200}
    >
      <div className="is-flex is-flex-direction-column is-align-items-center">
        <img
          style={{ width: 100 }}
          src={shakeImage}
        />
        <div className="has-text-centered is-size-7 has-text-weight-bold my-3">
          <p>スマホを振って応援するために</p>
          <p>「動作と方向」へのアクセスが必要です</p>
        </div>
        <button
          className="button is-primary is-fullwidth"
          onClick={onButtonPressed}
        >
          アクセスを許可する
        </button>
      </div>
    </Modal>
  </>
}

export default ShakeModal
