import { Routes, Route } from 'react-router-dom'

import MainScreen from 'screens/main'
import VoiceSelectScreen from 'screens/voice-select'

const Router = () => (
  <Routes>
    <Route path="/" element={<MainScreen />} />
    <Route path="/voices" element={<VoiceSelectScreen />} />
  </Routes>
)

export default Router
