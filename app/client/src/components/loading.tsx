import { BeatLoader } from 'react-spinners'

type Props = {
  size?: number
}
const Loading = (props: Props) => {
  const size = props.size ?? 15

  return <>
    <div className="is-flex is-justify-content-center">
      <BeatLoader size={size} margin={4} color="rgba(0,0,0,0.5)" />
    </div>
  </>
}

export default Loading
