import { combineReducers } from '@reduxjs/toolkit'

import AppReducer from 'slices/app'
import CategoryReducer from 'slices/category'
import SoundGroupReducer from 'slices/sound-group'
import VoltageReducer from 'slices/voltage'

const rootReducer = combineReducers({
  app: AppReducer,
  category: CategoryReducer,
  soundGroup: SoundGroupReducer,
  voltage: VoltageReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
