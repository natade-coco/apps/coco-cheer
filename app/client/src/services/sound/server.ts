import { Application, FeathersService, Service } from '@feathersjs/feathers'

import { Category, SoundGroup, Voltage } from 'types/sound'
import { serverApp } from 'services/server'
import SoundService from './index'

const parseCategory = (data: any): Category => ({
  id: data['id'],
  name: data['name'],
  provider: data['provider']
})

const parseSoundGroup = (data: any): SoundGroup => ({
  id: data['id'],
  name: data['name'],
  type: data['type'],
  categoryId: data['categoryId'],
})

export default class ServerSampleService implements SoundService {
  feathers: {
    category: FeathersService<Application, Service<any>>,
    soundGroup: FeathersService<Application, Service<any>>,
    cheer: FeathersService<Application, Service<any>>,
    voltage: FeathersService<Application, Service<any>>
  }

  constructor() {
    this.feathers = {
      category: serverApp.service('categories'),
      soundGroup: serverApp.service('sound_groups'),
      cheer: serverApp.service('cheers'),
      voltage: serverApp.service('voltage')
    }
  }

  getCategories = async (): Promise<Category[]> => {
    try {
      const response = await this.feathers.category.find()
      const categories = response.map((data: any) => parseCategory(data))
      return categories
    } catch (error) {
      throw error
    }
  }

  getSoundGroups = async (): Promise<SoundGroup[]> => {
    try {
      const response = await this.feathers.soundGroup.find()
      const soundGroups = response.map((data: any) => parseSoundGroup(data))
      return soundGroups
    } catch (error) {
      throw error
    }
  }

  cheer = async (soundGroupId: string): Promise<string> => {
    try {
      await this.feathers.cheer.create({ soundGroupId })
      return soundGroupId
    } catch (error) {
      throw error
    }
  }

  listenVoltage = (listener: (voltage: Voltage) => void) => {
    this.feathers.voltage.on('created', (data: Voltage) => {
      listener(data)
    })
  }
}
