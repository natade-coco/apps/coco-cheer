import { SoundGroup, Category, Voltage } from 'types/sound'

export default interface SampleService {
  getCategories: () => Promise<Category[]>
  getSoundGroups: () => Promise<SoundGroup[]>
  cheer: (soundGroupId: string) => Promise<string>
  listenVoltage: (listener: (voltage: Voltage) => void) => void
}
