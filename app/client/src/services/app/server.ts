import { Session } from 'types/app'
import AppService from './index'
import { serverApp } from 'services/server'

export default class ServerAppService implements AppService {
  getSession = async (jwt: string): Promise<Session> => {
    try {
      return serverApp?.service('authentication').create({
        strategy: 'natadecoco',
        token: jwt
      })
    } catch (error) {
      console.error('[getSession]', error)
      throw new Error('server')
    }
  }
}
