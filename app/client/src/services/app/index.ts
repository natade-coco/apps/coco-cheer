import { Session } from 'types/app'

export default interface AppService {
  getSession: (jwt: string) => Promise<Session>
}
