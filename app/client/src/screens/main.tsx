import { useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { useReward } from 'react-rewards'
import { AwesomeButton } from 'react-awesome-button'
import AwesomeButtonStyles from 'react-awesome-button/src/styles/styles.scss'

import './main.css'
import { logoImage, shakeTitleImage, voiceTitleImage } from 'assets/image'
import { AppDispatch } from 'app/store'
import { RootState } from 'app/root-reducer'
import SoundWave from 'components/sound-wave'
import { cheerByShake, cheerByPush, selectedVoiceSelector } from 'slices/sound-group'
import { categorySelectors } from 'slices/category'
import { levelSelector, startListening } from 'slices/voltage'

const REWARD_ID = 'reward'
const BUTTON_INTERVAL = 10    // [s]

type Props = {}
const MainScreen = (props: Props) => {
  return <>
    <div className="screen main">
      <Header />
      <ShakeSection />
      <VoiceSection />
      <span id={REWARD_ID} className="is-align-self-center" />
    </div>
  </>
}

const Header = () => {
  return <>
    <div className="header px-2">
      <img src={logoImage} />
    </div>
  </>
}

type TitleProps = {
  image: string
}
const Title = (props: TitleProps) => (
  <div className="title has-background-info m-0 px-1">
    <img src={props.image} />
  </div>
)

const ShakeSection = () => {
  const dispatch: AppDispatch = useDispatch()

  const onWavePressed = useCallback(() => {
    if (import.meta.env.DEV) {
      dispatch(cheerByShake())
    }
  }, [])

  useEffect(() => {
    dispatch(startListening())
  }, [])

  return <>
    <div className="shake">
      <Title image={shakeTitleImage} />
      <div className="flex-container px-5">スマホを振るたびに会場に歓声が流れます</div>
      <div
        className="wave"
        onClick={onWavePressed}
      >
        <SoundWave />
      </div>
      <div className="flex-container px-5">
        <p className="is-size-7 has-text-danger">スマホを投げ飛ばさないようご注意ください</p>
      </div>
    </div>
  </>
}

const VoiceSection = () => {
  const dispatch: AppDispatch = useDispatch()
  const navigate = useNavigate()
  const { reward } = useReward(REWARD_ID, 'emoji', { zIndex: 100, startVelocity: 50 })

  const voice = useSelector(selectedVoiceSelector)
  const category = useSelector((state: RootState) => categorySelectors.selectById(state, voice?.categoryId ?? ''))

  const [intervalCount, setIntervalCount] = useState(0)

  const isButtonEnabled = useMemo(() => (
    Boolean(voice) && (intervalCount <= 0)
  ), [voice, intervalCount])

  const onButtonPressed = useCallback(() => {
    if (isButtonEnabled) {
      dispatch(cheerByPush())
      reward()
      setIntervalCount(BUTTON_INTERVAL)
    }
  }, [isButtonEnabled])

  const onVoicePressed = useCallback(() => {
    navigate('./voices')
  }, [])

  useEffect(() => {
    if (intervalCount > 0) {
      setTimeout(() => {
        setIntervalCount(value => value - 1)
      }, 1000)
    }
  }, [intervalCount])

  return <>
    <div className="voice">
      <Title image={voiceTitleImage} />
      <div className="flex-container px-5">
        <p>ボタンを押すと特別な声援を送れます</p>
      </div>
      <div className="flex-container pb-3">
        <AwesomeButton
          cssModule={AwesomeButtonStyles}
          style={{ height: 100, width: 150 }}
          type="primary"
          disabled={!isButtonEnabled}
          onPress={onButtonPressed}
        >
          <div className="is-flex is-flex-direction-column has-text-light">
            {(intervalCount > 0) ? <>
              <p className="is-size-7">インターバル中です</p>
              <p className="is-size-4">{intervalCount}</p>
            </> : ' '}
          </div>
        </AwesomeButton>
      </div>
      <div className="flex-container px-5 py-3">
        <div
          className="columns select-button is-mobile is-vcentered"
          onClick={onVoicePressed}
        >
          <div className="column has-text-centered py-2">
            {category &&
              <p className="is-size-7 has-text-weight-bold has-text-info">
                {category.name}
              </p>
            }
            <p className="has-text-weight-bold has-text-primary">
              {voice ? voice.name : 'ボイスを選ぶ'}
            </p>
          </div>
          <div className="column is-narrow py-2">
            <span className="icon has-text-primary">
              <i className="fas fa-chevron-right" />
            </span>
          </div>
        </div>
      </div>
    </div>
  </>
}

export default MainScreen
