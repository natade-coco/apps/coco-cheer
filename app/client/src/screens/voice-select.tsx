import { useCallback, useEffect, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import './voice-select.css'
import { AppDispatch } from 'app/store'
import { categorySelectors } from 'slices/category'
import { selectedVoiceIdSelector, selectedVoiceSelector, selectVoice, voicesSelector } from 'slices/sound-group'
import { useNavigate } from 'react-router-dom'

type Props = {}
const VoiceSelectScreen = (props: Props) => {
  return <>
    <div className="screen voice-select">
      <Header />
      <Selection />
    </div>
  </>
}

const Header = () => {
  const navigate = useNavigate()

  const onBackPressed = useCallback(() => {
    navigate('./..')
  }, [])

  return <>
    <div className="header">
      <span
        className="icon is-large has-text-grey"
        onClick={onBackPressed}
      >
        <i className="fas fa-lg fa-chevron-left" />
      </span>
      <span>ボイスを選択</span>
    </div>
  </>
}

const Selection = () => {
  const dispatch: AppDispatch = useDispatch()
  const navigate = useNavigate()

  const categories = useSelector(categorySelectors.selectAll)
  const voices = useSelector(voicesSelector)
  const selectedVoice = useSelector(selectedVoiceSelector)

  const [selectedTab, setSelectedTab] = useState<string>()

  const tabs = useMemo(() => categories.map(category => ({
    id: category.id,
    title: category.name
  })), [categories])

  const targetVoices = useMemo(() => voices.filter(voice => (
    voice.categoryId === selectedTab
  )), [voices, selectedTab])

  const selectedCategory = useMemo(() => (
    categories.find(category => (category.id === selectedTab))
  ), [categories, selectedTab])

  const onVoiceSelected = useCallback((id: string) => {
    dispatch(selectVoice({ voiceId: id }))
      .then(() => {
        navigate('./..')
      })
  }, [])

  useEffect(() => {
    if (categories.length > 0) {
      if (selectedVoice) {
        setSelectedTab(selectedVoice.categoryId)
      } else {
        setSelectedTab(categories[0].id)
      }
    }
  }, [categories, selectedVoice])

  useEffect(() => {
    console.log(`***`, selectedTab)
    if (selectedTab) {
      document.getElementById(selectedTab)?.scrollIntoView()
    }
  }, [selectedTab])

  return <>
    <div className="selection">
      <div className="tabs is-boxed mb-0">
        {tabs.map(tab => (
          <ul key={tab.id} id={tab.id}>
            <li className={`${(tab.id === selectedTab) ? 'is-active' : ''}`}>
              <a
                className="is-size-7 has-text-weight-bold"
                onClick={() => setSelectedTab(tab.id)}
              >
                {tab.title}
              </a>
            </li>
          </ul>
        ))}
      </div>
      <div className="is-flex-grow-1">
        <div className="buttons p-2">
          {targetVoices.map(voice => (
            <button
              key={voice.id}
              className={`button is-primary ${(voice.id === selectedVoice?.id) ? '' : 'is-outlined has-background-white'} m-1`}
              onClick={() => onVoiceSelected(voice.id)}
            >
              <span className="has-text-weight-bold">
                {voice.name}
              </span>
            </button>
          ))}
        </div>
      </div>
      {selectedCategory &&
        <p className="provider is-size-7">
          ボイス提供: {selectedCategory.provider}
        </p>
      }
    </div>
  </>
}

export default VoiceSelectScreen
