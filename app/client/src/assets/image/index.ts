import logoImage from './coco-cheer-logo_tomato.png'
import shakeTitleImage from './shake_title.png'
import voiceTitleImage from './voice_title.png'
import shakeImage from './shake_color.png'

export {
  logoImage,
  shakeTitleImage,
  voiceTitleImage,
  shakeImage
}
