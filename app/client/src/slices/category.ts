import { createSlice, createAsyncThunk, PayloadAction, createEntityAdapter } from '@reduxjs/toolkit'

import { RootState } from 'app/root-reducer'
import { Category } from 'types/sound'
import SoundService from 'services/sound'
import ServerSoundService from 'services/sound/server'

const soundService: SoundService = new ServerSoundService()

// Entity Adapter

const entityAdapter = createEntityAdapter<Category>()

const initialState = entityAdapter.getInitialState()

// Selectors

const inputSelector = (state: RootState) => state.category

export const categorySelectors = entityAdapter.getSelectors(inputSelector)

// Thunks

export type GetCategoriesResponse = {
  categories: Category[]
}
export const getCategories = createAsyncThunk(
  'category/getCategories',
  async (): Promise<GetCategoriesResponse> => {
    try {
      const categories = await soundService.getCategories()
      return { categories }
    } catch (err) {
      throw err
    }
  }
)

// Slice

const slice = createSlice({
  name: 'category',
  initialState,
  reducers: {},
  extraReducers: builder => builder
    .addCase(
      getCategories.fulfilled,
      (state, { payload }: PayloadAction<GetCategoriesResponse>) => {
        entityAdapter.setAll(state, payload.categories)
      }
    )
})

export default slice.reducer
