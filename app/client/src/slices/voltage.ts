import { createSlice, PayloadAction, createSelector, createAsyncThunk, createAction } from '@reduxjs/toolkit'

import { RootState } from 'app/root-reducer'
import { Voltage, VoltageLevel } from 'types/sound'
import SoundService from 'services/sound'
import ServerSoundService from 'services/sound/server'

const soundService: SoundService = new ServerSoundService()

export interface State {
  level: VoltageLevel
}

const initialState: State = {
  level: 'zero'
}

// Selectors

const inputSelector = (state: RootState) => state.voltage

export const levelSelector = createSelector(
  inputSelector,
  (state: State) => state.level
)

// Actions

export const startListening = createAsyncThunk(
  'voltage/startListening',
  async (_, { dispatch }) => {
    try {
      soundService.listenVoltage(voltage => {
        console.log('[onVoltage] level', voltage.level)
        dispatch(onVoltage(voltage))
      })
    } catch (err) {
      throw err
    }
  }
)

export const onVoltage = createAction<Voltage>(
  'voltage/onVoltage'
)

// Slice

const slice = createSlice({
  name: 'voltage',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(
        onVoltage,
        (state, { payload }: PayloadAction<Voltage>) => {
          state.level = payload.level
        }
      )
  }
})

export default slice.reducer
