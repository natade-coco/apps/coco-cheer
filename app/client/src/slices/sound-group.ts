import { createSlice, createAsyncThunk, PayloadAction, createEntityAdapter, createSelector } from '@reduxjs/toolkit'

import { RootState } from 'app/root-reducer'
import { SoundGroup } from 'types/sound'
import SoundService from 'services/sound'
import ServerSoundService from 'services/sound/server'
import { categorySelectors } from './category'

const soundService: SoundService = new ServerSoundService()

// Entity Adapter

const entityAdapter = createEntityAdapter<SoundGroup>()

const extraState: {
  selectedVoiceId: string | undefined
} = {
  selectedVoiceId: undefined
}

const initialState = entityAdapter.getInitialState(extraState)

// Selectors

const inputSelector = (state: RootState) => state.soundGroup

export const soundGroupSelectors = entityAdapter.getSelectors(inputSelector)

export const voicesSelector = createSelector(
  soundGroupSelectors.selectAll,
  soundGroups => soundGroups.filter(soundGroup => (soundGroup.type === 'voice'))
)

export const applauseSelector = createSelector(
  soundGroupSelectors.selectAll,
  soundGroups => soundGroups.find(soundGroup => (soundGroup.type === 'applause'))
)

export const selectedVoiceIdSelector = createSelector(
  inputSelector,
  state => state.selectedVoiceId
)

export const selectedVoiceSelector = createSelector(
  [soundGroupSelectors.selectEntities, selectedVoiceIdSelector],
  (entities, id) => id ? entities[id] : undefined
)

// Thunks

export type GetSoundGroupsResponse = {
  soundGroups: SoundGroup[]
}
export const getSoundGroups = createAsyncThunk(
  'soundGroup/getSoundGroups',
  async (_, { getState, dispatch }): Promise<GetSoundGroupsResponse> => {
    const state = getState() as RootState

    try {
      const soundGroups = await soundService.getSoundGroups()

      // select first voice if not selected voice
      if (!state.soundGroup.selectedVoiceId) {
        const categoryIds = categorySelectors.selectIds(state)
        if (categoryIds.length > 0) {
          const firstVoice = soundGroups.find(soundGroup => (
            (soundGroup.type === 'voice') && (soundGroup.categoryId === categoryIds[0])
          ))
          if (firstVoice) {
            dispatch(selectVoice({ voiceId: firstVoice.id }))
          }
        }
      }

      return { soundGroups }
    } catch (err) {
      throw err
    }
  }
)

export type SelectVoice = {
  voiceId: string
}
export const selectVoice = createAsyncThunk(
  'soundGroup/selectVoice',
  async (value: SelectVoice): Promise<SelectVoice> => {
    return value
  }
)

export const cheerByShake = createAsyncThunk(
  'soundGroup/cheerByShake',
  async (_, { getState }) => {
    const state = getState() as RootState
    const applause = applauseSelector(state)
    if (!applause) throw new Error('internal')

    try {
      await soundService.cheer(applause.id)
    } catch (err) {
      throw err
    }
  }
)

export const cheerByPush = createAsyncThunk(
  'soundGroup/cheerByPush',
  async (_, { getState }) => {
    const state = getState() as RootState
    const id = selectedVoiceIdSelector(state)
    if (!id) throw new Error('internal')

    try {
      await soundService.cheer(id)
    } catch (err) {
      throw err
    }
  }
)

// Slice

const slice = createSlice({
  name: 'soundGroup',
  initialState,
  reducers: {},
  extraReducers: builder => builder
    .addCase(
      getSoundGroups.fulfilled,
      (state, { payload }: PayloadAction<GetSoundGroupsResponse>) => {
        entityAdapter.setAll(state, payload.soundGroups)
      }
    )
    .addCase(
      selectVoice.fulfilled,
      (state, { payload }: PayloadAction<SelectVoice>) => {
        state.selectedVoiceId = payload.voiceId
      }
    )
})

export default slice.reducer
