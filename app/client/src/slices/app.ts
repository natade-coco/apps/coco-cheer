import { createSlice, PayloadAction, createSelector, createAsyncThunk,
         isPending, isRejected, isFulfilled, createAction } from '@reduxjs/toolkit'

import { RootState } from 'app/root-reducer'
import { Session } from 'types/app'
import { getCategories } from './category'
import { getSoundGroups } from './sound-group'
import AppService from 'services/app'
import ServerAppService from 'services/app/server'
import PocketService from 'services/pocket'

const appService: AppService = new ServerAppService()
const pocket = new PocketService()

export interface State {
  pendingActions: string[],
  session: Session | undefined,
  hasPermissionRequested: boolean
}

const initialState: State = {
  pendingActions: [],
  session: undefined,
  hasPermissionRequested: false
}

// Selectors

const inputSelector = (state: RootState) => state.app

export const isLoadingSelector = createSelector(
  inputSelector,
  (state: State) => state.pendingActions.length > 0
)

export const hasPermissionRequestedSelector = createSelector(
  inputSelector,
  (state: State) => state.hasPermissionRequested
)

// Actions

export type SethasPermissionRequeted = {
  hasPermissionRequested: boolean
}
export const setHasPermissionRequested = createAction<SethasPermissionRequeted>(
  'app/setHasPermissionRequested'
)

// Thunks

export type AppStartResponse = {
  session: Session
}
export const appStart = createAsyncThunk(
  'app/start',
  async (): Promise<AppStartResponse> => {
    try {
      const jwt = await pocket.requestSignJWT()
      const session = await appService.getSession(jwt)
      return { session }
    } catch (error) {
      throw error
    }
  }
)

export const sync = createAsyncThunk(
  'app/sync',
  async (_, { dispatch }) => {
    try {
      await dispatch(getCategories())
      await dispatch(getSoundGroups())
    } catch (error) {
      throw error
    }
  }
)

// Slice

const getOriginalActionType = (action: PayloadAction<any>): string =>
  action.type.slice(0, action.type.lastIndexOf('/'))

const startLoading = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.add(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
}

const loadingSuccess = (state: State, action: PayloadAction<any>) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
}

const loadingFailed = (
  state: State,
  action: PayloadAction<any, string, any, any>
) => {
  const set = new Set(state.pendingActions)
  set.delete(getOriginalActionType(action))
  state.pendingActions = Array.from(set)
  console.error(`[loadingFailed]`, JSON.stringify(action.error?.message))
}

const slice = createSlice({
  name: 'app',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(
        appStart.fulfilled,
        (state, { payload }: PayloadAction<AppStartResponse>) => {
          state.session = payload.session
        }
      )
      .addCase(
        setHasPermissionRequested,
        (state, { payload }: PayloadAction<SethasPermissionRequeted>) => {
          state.hasPermissionRequested = payload.hasPermissionRequested
        }
      )
      .addMatcher(isPending, startLoading)
      .addMatcher(isFulfilled, loadingSuccess)
      .addMatcher(isRejected, loadingFailed)
  },
})

export default slice.reducer
