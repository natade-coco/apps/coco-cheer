import { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ShakeDetector from 'shake-detector'

import { AppDispatch } from 'app/store'
import Layout from 'components/layout'
import Router from 'components/router'
import Loading from 'components/loading'
import ShakeModal from 'components/modal/shake-modal'
import { appStart, hasPermissionRequestedSelector, sync } from 'slices/app'
import { cheerByShake } from 'slices/sound-group'
import { isAndroid } from 'utils/os'

const SHAKE_INTERVAL = 1500   // [ms]

const App = () => {
  const dispatch: AppDispatch = useDispatch()

  const hasPermissionRequested = useSelector(hasPermissionRequestedSelector)

  const [isAppReady, setIsAppReady] = useState(false)
  const [showsShakeModal, setShowsShakeModal] = useState(false)

  const onShakeModalCloseRequested = useCallback(() => {
    setShowsShakeModal(false)
  }, [])

  const onShake = useCallback(() => {
    console.log('[shake]', 'shaked')
    dispatch(cheerByShake())
  }, [])

  useEffect(() => {
    if (!isAndroid() && isAppReady && !hasPermissionRequested) {
      setShowsShakeModal(true)
    }
  }, [isAppReady, hasPermissionRequested])

  useEffect(() => {
    dispatch(appStart()).then(() => {
      dispatch(sync()).then(() => {
        setIsAppReady(true)
      })
    })
  }, [])

  useEffect(() => {
    const shake = new ShakeDetector({
      threshold: 5,
      debounceDelay: SHAKE_INTERVAL
    })

    console.log('[shake]', 'subscribe')
    shake
      .confirmPermissionGranted()
      .subscribe(onShake)
      .start()
    return () => {
      console.log('[shake]', 'unsubscribe')
      shake.unsubscribe(onShake)
    }
  }, [])

  return <>
    {isAppReady ? <>
      <Layout>
        <Router />
      </Layout>
      <ShakeModal
        isActive={showsShakeModal}
        onCloseRequested={onShakeModalCloseRequested}
      />
    </> : <>
      <LoadingScreen />
    </>}
  </>
}

const LoadingScreen = () => (
  <div className="screen is-justify-content-center">
    <Loading />
  </div>
)

export default App
