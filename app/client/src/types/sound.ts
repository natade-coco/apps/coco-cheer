export type Category = {
  id: string,
  name: string,
  provider: string | undefined
}

export type SoundGroup = {
  id: string,
  name: string,
  type: 'voice' | 'applause'
  categoryId: string
}

export type VoltageLevel = 'zero' | 'low' | 'mid' | 'high'

export type Voltage = {
  level: VoltageLevel
}
