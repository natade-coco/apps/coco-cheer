import ReactDOM from 'react-dom/client'
import { HashRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import Modal from 'react-modal'
import '@fortawesome/fontawesome-free/js/all.js'

import App from './app'
import store from 'app/store'
import './styles.scss'


Modal.setAppElement('#root')

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <Provider store={store}>
    <HashRouter>
      <App />
    </HashRouter>
  </Provider>
)
