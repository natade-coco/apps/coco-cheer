import { Directus, TypeMap } from '@directus/sdk'
import fs from 'fs'

import jsonData from './data.json'

const COLLECTION_NAME = 'cheer_sounds'

export async function seed(client: Directus<TypeMap>) {
  console.log(`[${COLLECTION_NAME}.seed]`, 'start')

  try {
    await createItemsFromFiles(client)
    // await client.items(COLLECTION_NAME).createMany(jsonData)
  } catch (err) {
    console.error(`[${COLLECTION_NAME}.seed]`, err)
  }

  console.log(`[${COLLECTION_NAME}.seed] completed`)
}

export async function backup(client: Directus<TypeMap>, path: string) {
  console.log(`[${COLLECTION_NAME}].backup]`, 'start')

  try {
    const data = (await client.items(COLLECTION_NAME).readMany()).data
    if (data) {
      fs.writeFileSync(path + '/data.json', JSON.stringify(data, null, 4))
      console.log(`[${COLLECTION_NAME}].backup] completed`)
    } else {
      console.log(`[${COLLECTION_NAME}.backup] collection not exists`)
    }
  } catch (err) {
    console.error(`[${COLLECTION_NAME}.backup]`, err)
  }
}

const createItemsFromFiles = async (client: Directus<TypeMap>) => {
  try {
    const files = await client.files.readMany({ limit: -1 })
    const sounds = files.data!
      .filter(file => (file['type'] === 'audio/wav'))
      .map(file => ({
        id: file['id'],
        file: file['id'],
        volume: 0,
        name: file['title']
      }))
    await client.items(COLLECTION_NAME).createMany(sounds)
  } catch (err) {
    throw err
  }
}
