import { Directus, TypeMap } from '@directus/sdk'

import * as CheerSounds from './01_cheer_sounds'
import * as CheerCategories from './02_cheer_categories'
import * as CheerSoundGroups from './03_cheer_sound_groups'

export async function seed(client: Directus<TypeMap>, internalURL: string | undefined) {
  CheerSounds.seed(client)
  CheerCategories.seed(client)
  CheerSoundGroups.seed(client)
}

export async function backup(client: Directus<TypeMap>) {
  CheerSounds.backup(client, './src/tasks/seeds/01_cheer_sounds')
  CheerCategories.backup(client, './src/tasks/seeds/02_cheer_categories')
  CheerSoundGroups.backup(client, './src/tasks/seeds/03_cheer_sound_groups')
}
