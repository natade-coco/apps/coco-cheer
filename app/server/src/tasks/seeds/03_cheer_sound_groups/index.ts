import { Directus, TypeMap } from '@directus/sdk'
import fs from 'fs'

import jsonData from './data.json'

const COLLECTION_NAME = 'cheer_sound_groups'

export async function seed(client: Directus<TypeMap>) {
  console.log(`[${COLLECTION_NAME}.seed]`, 'start')

  try {
    await client.items(COLLECTION_NAME).createMany(jsonData)
  } catch (err) {
    console.error(`[${COLLECTION_NAME}.seed]`, err)
  }

  console.log(`[${COLLECTION_NAME}.seed] completed`)
}

export async function backup(client: Directus<TypeMap>, path: string) {
  console.log(`[${COLLECTION_NAME}].backup]`, 'start')

  try {
    const data = (await client.items(COLLECTION_NAME).readMany()).data
    if (data) {
      fs.writeFileSync(path + '/data.json', JSON.stringify(data, null, 4))
      console.log(`[${COLLECTION_NAME}].backup] completed`)
    } else {
      console.log(`[${COLLECTION_NAME}.backup] collection not exists`)
    }
  } catch (err) {
    console.error(`[${COLLECTION_NAME}.backup]`, err)
  }
}
