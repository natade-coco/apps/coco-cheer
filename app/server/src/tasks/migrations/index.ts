import { Directus, TypeMap } from "@directus/sdk";

import * as CheerSounds from "./01_cheer_sounds";
import * as CheerCategories from "./02_cheer_categories";
import * as CheerSoundGroups from "./03_cheer_sound_groups";
import * as CheerSoundGroupsCheerSounds from "./04_cheer_sound_groups_cheer_sounds";

export async function setup(client: Directus<TypeMap>) {
  await CheerSounds.setup(client);
  await CheerCategories.setup(client);
  await CheerSoundGroups.setup(client);
  await CheerSoundGroupsCheerSounds.setup(client);
}

export async function teardown(client: Directus<TypeMap>) {
  await CheerSounds.teardown(client);
  await CheerCategories.teardown(client);
  await CheerSoundGroups.teardown(client);
  await CheerSoundGroupsCheerSounds.teardown(client);
}

export async function dump(client: Directus<TypeMap>) {
  await CheerSounds.dump(client, "./src/tasks/migrations/01_cheer_sounds");
  await CheerCategories.dump(client, "./src/tasks/migrations/02_cheer_categories");
  await CheerSoundGroups.dump(client, "./src/tasks/migrations/03_cheer_sound_groups");
  await CheerSoundGroupsCheerSounds.dump(client, "./src/tasks/migrations/04_cheer_sound_groups_cheer_sounds");
}
