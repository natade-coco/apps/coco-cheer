import { Directus, TypeMap } from "@directus/sdk";

import jsonData from "./data.json";
import relationData from "./relations.json";

const collection = { name: "cheer_sound_groups_cheer_sounds", data: jsonData };
const relation = { name: "cheer_sound_groups_cheer_sounds_relations", data: relationData };

export async function setup(client: Directus<TypeMap>) {
  const check = await client.collections
    .readOne(collection.name)
    .catch(console.log);
  if (check) {
    console.log(collection.name, "collection exists");
  } else {
    const result = await client.collections
      .createOne(collection.data)
      .catch(console.log);
    console.log(collection.name, "collection created");
    await Promise.all(
      (relation.data as any[]).map(item => {
        return client.relations.createOne(item).catch(console.log);
      })
    );
    console.log(collection.name, "relation created");
  }
}

export async function teardown(client: Directus<TypeMap>) {
  const check = await client.collections
    .readOne(collection.name)
    .catch(console.log);
  if (check) {
    const result = await client.collections
      .deleteOne(collection.name)
      .catch(console.log);
    console.log(collection.name, "collection deleted");
  }
}

export async function dump(client: Directus<TypeMap>, path: string) {
  var fs = require("fs");

  const [cData, fData] = await Promise.all([
    client.collections.readOne(collection.name).catch(console.log),
    client.fields.readMany(collection.name),
  ]);
  if (cData && fData) {
    let fields = (fData as any).map((item: any) => {
      delete item.meta.id;
      return item;
    });
    (cData as any).fields = fields;

    fs.writeFileSync(
      path + "/data.json",
      JSON.stringify(cData, null, "    ")
    );
    console.log(collection.name, "collection dump");
  }
  let rData: any = await client.relations.readMany(collection.name);
  if (rData) {
    rData = (rData as any[]).map((item) => {
      delete item.meta.id;
      return item;
    });

    fs.writeFileSync(
      path + "/relations.json",
      JSON.stringify(rData, null, "    ")
    );
    console.log(relation.name, "relation dump");
  }
}
