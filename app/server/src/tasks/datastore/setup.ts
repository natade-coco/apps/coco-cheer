import { makeClient } from "./utils";
import { setup } from "../migrations";

(async function () {
  const client = await makeClient();
  setup(client);
})();
