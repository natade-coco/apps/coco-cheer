import { Directus, Auth } from "@directus/sdk";
import { SDK } from "@natade-coco/hub-sdk";

import dotenv from "dotenv";
import { DIDDocument } from "@natade-coco/hub-sdk/dist/Type";
dotenv.config();

export const makeClient = async () => {
  const url = process.env.DIRECTUS_URL || "";
  const id = process.env.DIRECTUS_ID || "";
  const pw = process.env.DIRECTUS_SECRET || "";
  const { transport, storage } = new Directus(url);
  const auth = new Auth({ transport, autoRefresh: false, storage });
  const directus = new Directus(url, { auth, transport, storage });
  await directus.auth.login({ email: id, password: pw });
  return directus;
};

export const fetchAppIdentity = async () => {
  const id = process.env.NATADECOCO_ID || "";
  const secret = process.env.NATADECOCO_SECRET || "";
  const env = process.env.NODE_ENV || "prd";
  try {
    const client = await SDK.init({
      id,
      secret,
      test: env === "stg",
    });
    return client.Resolve(id);
  } catch (err) {
    throw err;
  }
};

export const getServiceID = (doc: DIDDocument, type: string) => {
  const svc = doc.service.filter((svc) => svc.type === type)[0];
  const regex = /[devices|apps]\/(.+)\/spoke/;
  const result = svc.serviceEndpoint.match(regex);
  return result ? result[1] : "";
};

export const makeInternalURL = async () => {
  try {
    const identity = await fetchAppIdentity();
    const deviceID = getServiceID(identity, "DeviceSpoke");
    const appID = getServiceID(identity, "AppSpoke");
    const internalURL = `http://${appID}.${appID}-${deviceID}.svc.cluster.local:3030`;
    return internalURL;
  } catch (err) {
    return undefined;
  }
};
