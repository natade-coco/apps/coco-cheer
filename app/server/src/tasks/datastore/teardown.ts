import { makeClient } from './utils';
import { teardown } from '../migrations';
(async function () {
  const client = await makeClient();
  await teardown(client);
  client.auth.logout();
})();
