import { makeClient } from "./utils";
import { backup } from "../seeds";

(async function () {
  const client = await makeClient();
  backup(client);
})();
