import { makeClient, makeInternalURL } from "./utils";
import { seed } from "../seeds";

(async function () {
  const internalURL = await makeInternalURL();
  const client = await makeClient();
  seed(client, internalURL);
})();
