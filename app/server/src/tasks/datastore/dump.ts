import { makeClient } from "./utils";
import { dump } from "../migrations";

(async function () {
  const client = await makeClient();
  dump(client);
})();
