import compress from "compression";
import helmet from "helmet";
import cors from "cors";

import { feathers } from "@feathersjs/feathers";
import configuration from "@feathersjs/configuration";
import * as express from "@feathersjs/express";
import history from "connect-history-api-fallback";
import dotenv from "dotenv";

import { Application } from "./declarations";
import logger from "./logger";
import middleware from "./middleware";
import services from "./services";
import channels from "./channels";
import authentication from "./authentication";
import socketio from "./socketio";
import { datastore } from "./datastore";
import hub from "./hub";
// Don't remove this comment. It's needed to format import lines nicely.

const app: Application = express.default(feathers());

// Load app configuration
app.configure(configuration());

// Load from .env
dotenv.config();
app.set("directus_url", process.env.DIRECTUS_URL);
app.set("directus_id", process.env.DIRECTUS_ID);
app.set("directus_secret", process.env.DIRECTUS_SECRET);
app.set("natadecoco_id", process.env.NATADECOCO_ID);
app.set("natadecoco_secret", process.env.NATADECOCO_SECRET);
app.set("node_env", process.env.NODE_ENV || "prd");
app.set("exec_env", process.env.EXEC_ENV || "global");
app.set("applause_url", process.env.APPLAUSE_URL || "http://applause.default.svc.cluster.local:8080");
app.set("audience_size", Number(process.env.AUDIENCE_SIZE) || 500);
app.set("duration_ms", 1500);
app.set("decimation_rate", Number(process.env.DECIMATION_RATE) || 0.2);
app.set("test_audience_size", Number(process.env.TEST_AUDIENCE_SIZE) || 0);

// Enable history api fallback
app.use(history());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet({ contentSecurityPolicy: false }));
app.use(cors());
app.use(compress());
app.use(express.json({ limit: "10mb" }));
// resreqのbodyサイズ上限10MB
app.use(express.urlencoded({ extended: true, limit: "10mb" }));
// Host the public folder
app.use("/", express.static(app.get("public")));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());
app.configure(hub);
app.configure(datastore);

// Configure other middleware (see `middleware/index.ts`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.ts`)
app.configure(services);
// Set up event channels (see channels.ts)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));

export default app;
