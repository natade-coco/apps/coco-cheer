import logger from "./logger";
import app from "./app";

const START_DELAY = 3000;

const port = app.get("port");
setTimeout(() => {
  app.listen(port).then(() => {
    logger.info(`Feathers application started on http://${app.get("host")}:${port}`);
  });
}, START_DELAY);

process.on("unhandledRejection", (reason, p) => {
  console.log({ reason, p });
  logger.error("Unhandled Rejection at: Promise ", p, reason);
});
