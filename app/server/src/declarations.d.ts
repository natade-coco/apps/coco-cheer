import { AuthenticationService } from "@feathersjs/authentication/lib";
import { Application as ExpressFeathers } from "@feathersjs/express";

import { Categories } from "./services/categories/categories.class";
import { SoundGroups } from "./services/sound_groups/sound_groups.class";
import { Cheers } from "./services/cheers/cheers.class";
import { CheerChanged } from "./services/cheer_changed/cheer_changed.class";
import { Voltage } from "./services/voltage/voltage.class";

// A mapping of service names to types. Will be extended in service files.
export interface ServiceTypes {
  "/authentication": AuthenticationService;
  "/categories": Categories;
  "/sound_groups": SoundGroups;
  "/cheers": Cheers;
  "/cheer_changed": CheerChanged;
  "/voltage": Voltage;
}
// The application instance type that will be used everywhere else
export type Application = ExpressFeathers<ServiceTypes>;
