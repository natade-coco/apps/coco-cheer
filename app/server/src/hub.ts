import { SDK as Hub } from '@natade-coco/hub-sdk'
import { DIDDocument } from '@natade-coco/hub-sdk/dist/Type'

import { Application } from './declarations'

let hub: Hub
let doc: DIDDocument
let isSTG: boolean

export default async function (app: Application) {
  const id = app.get('natadecoco_id')
  const secret = app.get('natadecoco_secret')
  isSTG = (app.get('node_env') === 'stg')
  if (app.get('exec_env') !== 'local') {
    hub = await Hub.init({ id, secret, test: isSTG })
    doc = await hub.Resolve(id)
  }
}

export const extractServiceID = (type: string) => {
  const svc = doc.service.filter((svc) => svc.type === type)[0]
  const regex = /[devices|apps]\/(.+)\/spoke/
  const result = svc.serviceEndpoint.match(regex)
  return result ? result[1] : ''
}

export const getInternalURL = async () => {
  const appId = extractServiceID('AppSpoke')
  const deviceId = extractServiceID('DeviceSpoke')
  return `http://${appId}.${appId}-${deviceId}.svc.cluster.local:3030`
}
