import socketio from '@feathersjs/socketio'

import logger from './logger'

const setupSocketIO = () => (
  socketio(io => {
    io.use((socket, next) => {
      // @ts-ignore
      socket.feathers.clientId = socket.client['id']
      next()
    })

    io.on('connection', socket => {
      const clientId: string = socket.client['id']
      logger.info('socket connected', { clientId })

      socket.on('ping', callback => {
        callback()
      })

      socket.on('disconnect', () => {
        logger.info('socket disconnected', { clientId })
      })
    })
  })
)

export default setupSocketIO
