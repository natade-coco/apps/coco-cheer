import { Directus, TypeMap } from "@directus/sdk";
import { Application } from "@feathersjs/feathers";
import { ServiceTypes } from "./declarations";

let client: Directus<TypeMap>;
let url: string;
let id: string;
let pw: string;

export const datastore = function (app: Application<ServiceTypes>) {
  url = app.get("directus_url");
  id = app.get("directus_id");
  pw = app.get("directus_secret");
  client = new Directus(url);
  client.auth.login({ email: id, password: pw })
    .catch(err => {
      console.log("initial login error", { err });
      process.exit(1);
    });
};

export const recreate = async () => {
  client = new Directus(url);
  await client.auth
    .login({ email: id, password: pw })
    .then((result) => {
      console.log("recreate success", { result });
    })
    .catch((err) => {
      console.log("recreate error", { err });
    });
  return client;
};

export const Datastore = function () {
  return client;
};
