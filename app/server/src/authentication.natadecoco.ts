import { Params } from "@feathersjs/feathers";
import { AuthenticationBaseStrategy, AuthenticationResult } from "@feathersjs/authentication";
import jwtDecode from "jwt-decode";

export class NatadeCOCOStrategy extends AuthenticationBaseStrategy {
  get configuration() {
    const authConfig = this.authentication?.configuration;
    const config = super.configuration || {};

    return {
      service: authConfig.service,
      entity: authConfig.entity,
      entityId: authConfig.entityId,
      errorMessage: "Invalid token",
      tokenField: config.tokenField,
      ...config,
    };
  }

  async authenticate(data: AuthenticationResult, params: Params) {
    const { tokenField } = this.configuration;
    const token = data[tokenField];
    const userId = (this.app?.get('exec_env') === 'local')
      ? 'local-user'
      : jwtDecode<{ iss: string }>(token).iss;
    return {
      authentication: { strategy: this.name },
      app: {
        id: this.app?.get("natadecoco_id"),
      },
      user: {
        id: userId,
      },
    };
  }
}
