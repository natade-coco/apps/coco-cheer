import '@feathersjs/transport-commons'
import { HookContext } from '@feathersjs/feathers'
import { Data as VoltageData } from './services/voltage/voltage.class'

import { Application } from './declarations'

export default function (app: Application): void {
  app.on('connection', connection => {
    app.channel('voltage').join(connection)
  })

  app.service('voltage').publish('created', (data: VoltageData, context: HookContext) => {
    return app.channel('voltage')
  })
}
