// Initializes the `cheer_changed` service on path `/cheer_changed`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { CheerChanged } from './cheer_changed.class';
import hooks from './cheer_changed.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'cheer_changed': CheerChanged & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/cheer_changed', new CheerChanged(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('cheer_changed');

  service.hooks(hooks);
}
