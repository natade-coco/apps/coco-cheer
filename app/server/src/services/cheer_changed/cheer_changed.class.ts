import { Id, NullableId, Paginated, Params, ServiceMethods } from '@feathersjs/feathers';

import { Application } from '../../declarations';
import { getInternalURL } from "../../hub";
import { Datastore } from '../../datastore';
import logger from '../../logger';

interface Data {}

interface ServiceOptions {}

export class CheerChanged implements ServiceMethods<Data> {
  app: Application;
  options: ServiceOptions;

  constructor(options: ServiceOptions = {}, app: Application) {
    this.options = options;
    this.app = app;
  }

  async setup(app: Application): Promise<void> {
    const name = 'cheer_changed'
    const collection = 'webhooks'

    const datastore = Datastore()
    const { data: webhooks } = await datastore.transport.get(collection)
    for (const wh of webhooks) {
      if (wh['name'] === name) {
        logger.info('webhook already exist')
        return
      }
    }
  
    try {
      const url = (await getInternalURL()) + '/cheer_changed'
      const webhook = {
        name,
        method: 'POST',
        url,
        status: 'active',
        data: false,
        actions: ['create', 'update', 'delete'],
        collections: ['cheer_sounds', 'cheer_categories', 'cheer_sound_groups']
      }
      await datastore.transport.post(collection, webhook)
      logger.info('webhook created')
    } catch (err) {
      logger.error(`webhook create error ${err}`)
    }
  }

  async find(params?: Params): Promise<Data[]> {
    throw new Error('not implemented')
  }

  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('not implemented')
  }

  async create(data: Data, params?: Params): Promise<Data> {
    logger.info('cheer datastore changed', data)
    this.app.setup()
    return data
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('not implemented')
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('not implemented')
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('not implemented')
  }
}
