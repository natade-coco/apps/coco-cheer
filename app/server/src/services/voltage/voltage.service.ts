// Initializes the `voltage` service on path `/voltage`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Voltage } from './voltage.class';
import hooks from './voltage.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'voltage': Voltage & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/voltage', new Voltage(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('voltage');

  service.hooks(hooks);
}
