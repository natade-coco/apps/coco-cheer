import { Id, NullableId, Params, ServiceMethods } from '@feathersjs/feathers';

import { Application } from '../../declarations';
import logger from '../../logger';

export type Level = 'zero' | 'low' | 'mid' | 'high'

export const levelByRate = (rate: number): Level => {
  if (rate <= 0) {
    return 'zero'
  } else if (rate < 0.2) {
    return 'low'
  } else if (rate < 0.5) {
    return 'mid'
  } else {
    return 'high'
  }
}

export type Data = {
  level: Level
}

export type ServiceOptions = {}

export class Voltage implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions

  constructor(options: ServiceOptions, app: Application) {
    this.options = options
    this.app = app
  }

  async setup() {}

  async find(params?: Params): Promise<Data[]> {
    throw new Error('notImplemented')
  }

  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create(data: Data, params?: Params): Promise<Data> {
    const { level } = data
    logger.info('voltage.create', { voltage: level })
    return data
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }
}
