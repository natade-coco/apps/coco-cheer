// Initializes the `buttons` service on path `/sound_groups`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { SoundGroups } from './sound_groups.class';
import hooks from './sound_groups.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'sound_groups': SoundGroups & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/sound_groups', new SoundGroups(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('sound_groups');

  service.hooks(hooks);
}
