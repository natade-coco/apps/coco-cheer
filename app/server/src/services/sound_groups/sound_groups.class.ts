import { Service, MemoryServiceOptions } from 'feathers-memory'
import { Directus, IItems } from '@directus/sdk'
import Axios, { AxiosInstance } from 'axios'
import FormData from 'form-data'

import { Application } from '../../declarations'
import { Datastore } from '../../datastore'
import logger from '../../logger'

export type Sound = {
  id: string,
  name: string | undefined,
  volume: number
}

export type SoundGroup = {
  id: string,
  name: string,
  type: 'voice' | 'applause',
  categoryId: string,
  sounds: Sound[]
}

export class SoundGroups extends Service<SoundGroup> {
  app: Application
  datastore: Directus<any>
  items: IItems<any>
  datastoreAssets: AxiosInstance
  applause: AxiosInstance

  constructor(options: Partial<MemoryServiceOptions>, app: Application) {
    super(options)
    this.app = app
    this.datastore = Datastore()
    this.items = this.datastore.items('cheer_sound_groups')
    this.datastoreAssets = this.createDatastoreAssets()
    this.applause = Axios.create({ baseURL: app.get('applause_url') })
  }

  createDatastoreAssets(): AxiosInstance {
    const axios = Axios.create({
      baseURL: this.app.get('directus_url') + '/assets',
      responseType: 'arraybuffer'
    })
    axios.interceptors.request.use(request => {
      if (request.headers) {
        request.headers['Authorization'] = `Bearer ${this.datastore.auth.token}`
      }
      return request
    })
    return axios
  }

  async setup(app: Application) {
    try {
      await this.sync()
    } catch (err) {
      throw err
    }
  }

  async sync(): Promise<void> {
    try {
      // fetch from datastore
      const result = await this.items.readMany({ limit: -1, fields: '*,sounds.cheer_sounds_id.*' })
      const data: any = result.data!
      await this.registerToApplause(data)
      await this.removeMemory()
      await this.storeToMemory(data)
    } catch (err) {
      throw err
    }
  }

  async registerToApplause(data: any[]): Promise<void> {
    for (const d of data) {
      for (const s of d['sounds']) {
        const sound = s['cheer_sounds_id']

        try {
          const id = sound['id']
          const file = await this.getDatastoreAsset(sound['file'])

          const formData = new FormData()
          formData.append('id', id)
          formData.append('file', file, { filename: `${id}.wav` })
          formData.append('volume', sound['volume'])

          await this.applause.post('/sounds', formData, {
            headers: formData.getHeaders()
          })
          logger.info('POST /sounds OK', { soundId: id })
        } catch (err) {
          throw err
        }
      }
    }
  }

  async getDatastoreAsset(id: string): Promise<ArrayBuffer> {
    try {
      const response = await this.datastoreAssets.get(`/${id}`)
      const file: ArrayBuffer = response.data
      return file
    } catch (err) {
      throw err
    }
  }

  async removeMemory() {
    try {
      const soundGroups = await this.find() as SoundGroup[]
      await Promise.all(soundGroups.map(soundGroup => this.remove(soundGroup.id)))
    } catch (err) {
      throw err
    }
  }

  async storeToMemory(data: any[]): Promise<void> {
    const soundGroups: SoundGroup[] = data.map((d: any) => {
      const sounds: Sound[] = d['sounds'].map((s: any) => {
        const sound = s['cheer_sounds_id']
        return {
          id: sound['id'],
          name: sound['name'],
          volume: sound['volume']
        }
      })
      return {
        id: d['id'],
        name: d['name'],
        type: d['type'],
        categoryId: d['category'],
        sounds,
      }
    })
    for (const soundGroup of soundGroups) {
      await this.create(soundGroup)
    }
  }
}
