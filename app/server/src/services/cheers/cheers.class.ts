import { FeathersService, Id, NullableId, Params, ServiceMethods, ServiceAddons, Service } from '@feathersjs/feathers'
import { TooManyRequests } from '@feathersjs/errors'
import Axios, { AxiosInstance } from 'axios'

import { Application } from '../../declarations'
import logger from '../../logger'
import { SoundGroup, SoundGroups } from '../sound_groups/sound_groups.class'
import { levelByRate, Voltage } from '../voltage/voltage.class'

interface Data {
  soundGroupId: string
}

interface ServiceOptions {
  audienceSize: number,
  duration: number,
  guardInterval: number
}

export class Cheers implements ServiceMethods<Data> {
  app: Application
  options: ServiceOptions
  soundGroupsService: FeathersService<Application, SoundGroups & ServiceAddons<any, Service<any, any>>>
  applause: AxiosInstance
  voltageService: FeathersService<Application, Voltage & ServiceAddons<any, Service<any, any>>>

  guardTimer?: NodeJS.Timer
  voltageTimer?: NodeJS.Timer
  voltageCount: number
  testTimer?: NodeJS.Timer

  constructor(options: ServiceOptions, app: Application) {
    this.options = options
    this.app = app
    this.soundGroupsService = app.service('sound_groups')
    this.applause = Axios.create({ baseURL: app.get('applause_url') })
    this.voltageService = app.service('voltage')

    this.voltageCount = 0
  }

  async setup(app: Application, path: string): Promise<void> {
    const { testTimer } = this
    if (testTimer) {
      clearInterval(testTimer)
    }

    const testAudienceSize: number = app.get("test_audience_size")
    if (testAudienceSize) {
      const wait = 3000
      const interval = this.options.duration / testAudienceSize
      logger.info(`test will start in ${wait}ms ...`, { testAudienceSize, interval })
      await new Promise(resolve => setTimeout(resolve, wait))

      const soundGroups = await this.soundGroupsService.find() as SoundGroup[]
      const soundGroupIds = soundGroups
        .filter(soundGroup => (soundGroup.type === 'applause'))
        .map(soundGroup => soundGroup.id)

      this.testTimer = setInterval(() => {
        const soundGroupId = soundGroupIds[Math.floor(Math.random() * soundGroupIds.length)]
        this.create({ soundGroupId })
      }, interval)
    }
  }

  async find(params?: Params): Promise<Data[]> {
    throw new Error('notImplemented')
  }

  async get(id: Id, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async create(data: Data, params?: Params): Promise<Data> {
    const { soundGroupId } = data

    try {
      const soundGroup = await this.soundGroupsService.get(soundGroupId, params)

      if (soundGroup.type === 'applause') {
        // handle voltage
        if (this.voltageTimer) {
          this.voltageCount ++
        } else {
          this.voltageService.create({ level: 'low' })
          this.startCount()
        }

        // limit requests
        if (this.guardTimer) {
          const error = new TooManyRequests()
          logger.info('cheers.create', { error })
          throw error
        } else {
          this.guardTimer = setTimeout(() => {
            this.guardTimer = undefined
          }, this.options.guardInterval)
        }
      }

      // send request to applause server
      const { sounds } = soundGroup
      const sound = sounds[Math.floor(Math.random() * sounds.length)]
      const stretch = 0.95 + (Math.random() * 0.1)  // 0.95 〜 1.05
      const pan = -1 + (Math.random() * 2)  // -1 〜 1
      logger.info('cheers.create', { soundGroup: soundGroupId, sound: sound.id, user: params?.user?.id })
      await this.applause.post(`/sounds/${sound.id}/play`, {
        stretch,
        pan
      })
    } catch (err: any) {
      logger.error('cheers.create', { message: err.message })
    }

    return data;
  }

  async update(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async patch(id: NullableId, data: Data, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async remove(id: NullableId, params?: Params): Promise<Data> {
    throw new Error('notImplemented')
  }

  async startCount() {
    this.voltageTimer = setTimeout(() => {
      const rate = this.voltageCount / this.options.audienceSize
      const level = levelByRate(rate)
      this.voltageService.create({ level })

      if (this.voltageCount > 0) {
        this.startCount()
      } else {
        this.voltageTimer = undefined
      }
      this.voltageCount = 0
    }, this.options.duration)
  }
}
