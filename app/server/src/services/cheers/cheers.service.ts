// Initializes the `cheers` service on path `/cheers`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { Cheers } from './cheers.class';
import hooks from './cheers.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'cheers': Cheers & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const audienceSize: number = app.get('audience_size')
  const duration: number = app.get('duration_ms')
  const decimationRate: number = app.get('decimation_rate')
  const guardInterval = duration / audienceSize / decimationRate

  const options = {
    audienceSize,
    duration,
    guardInterval
  };

  // Initialize our service with any options it requires
  app.use('/cheers', new Cheers(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('cheers');

  service.hooks(hooks);
}
