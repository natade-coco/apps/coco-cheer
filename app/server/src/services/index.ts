import { Application } from "../declarations";
import categories from './categories/categories.service';
import soundGroups from './sound_groups/sound_groups.service';
import voltage from './voltage/voltage.service';
import cheers from './cheers/cheers.service';
import cheerChanged from './cheer_changed/cheer_changed.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(categories);
  app.configure(soundGroups);
  app.configure(voltage);
  app.configure(cheers);
  app.configure(cheerChanged);
}
