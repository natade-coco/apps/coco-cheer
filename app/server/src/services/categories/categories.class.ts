import { Service, MemoryServiceOptions } from 'feathers-memory'
import { IItems } from '@directus/sdk'

import { Application } from '../../declarations'
import { Datastore } from '../../datastore'

type Category = {
  id: string,
  name: string,
  provider: string | undefined
}

export class Categories extends Service<Category> {
  app: Application
  items: IItems<any>

  constructor(options: Partial<MemoryServiceOptions>, app: Application) {
    super(options)
    this.app = app
    const datastore = Datastore()
    this.items = datastore.items('cheer_categories')
  }

  async setup(app: Application) {
    try {
      await this.sync()
    } catch (err) {
      throw err
    }
  }

  async sync() {
    try {
      const result = await this.items.readMany({ limit: -1 })
      const data: any = result.data!
      await this.removeMemory()
      await this.storeToMemory(data)
    } catch (err) {
      throw err
    }
  }

  async removeMemory() {
    try {
      const categories = await this.find() as Category[]
      await Promise.all(categories.map(category => this.remove(category.id)))
    } catch (err) {
      throw err
    }
  }

  async storeToMemory(data: any[]) {
    try {
      const categories: Category[] = data.map(d => ({
        id: d['id'],
        name: d['name'],
        provider: d['provider']
      }))
      for (const category of categories) {
        await this.create(category)
      }
    } catch (err) {
      throw err
    }
  }
}
