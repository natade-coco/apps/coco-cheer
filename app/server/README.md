# Server App

## 開発手順

一部の環境変数を取得するために、一度管理コンソールからアプリを登録しておく必要があります。

### 1. 環境変数ファイルを作成

`server` フォルダ直下に `.env` ファイルを作成し、以下の環境変数を設定します。

| 変数名 | 説明 | デフォルト | 備考 |
| :-- | :-- | :-- | :-- |
| DIRECTUS_URL | Directus URL | | 管理コンソールのデータストア設定から取得 |
| DIRECTUS_ID | Directus ID | | 管理コンソールのデータストア設定から取得 |
| DIRECTUS_SECRET | Directus Secret | | 管理コンソールのデータストア設定から取得 |
| NATADECOCO_ID | natadeCOCO ID | | 管理コンソールのアプリ詳細から取得 |
| NATADECOCO_SECRET | natadeCOCO Secret | | 管理コンソールのアプリ詳細から取得 |
| NODE_ENV | stg または prd | prd | |
| EXEC_ENV | local または global | global | localの場合、ユーザー認証をスキップ |
| APPLAUSE_URL | ApplauseのURL | http://applause.default.svc.cluster.local:8080 | |
| AUDIENCE_SIZE | 想定観客数 | 500 | |
| DECIMATION_RATE | 再生する歓声を間引くための係数 | 0.2 | |
| TEST_AUDIENCE_SIZE | テスト用の観客数 | 0 | |

#### `.env` ファイルの例

```
DIRECTUS_URL=https://api.xxxxxxx.natade-coco.com
DIRECTUS_ID=xxxxxxx@xxxxxxx.xxxxxxx
DIRECTUS_SECRET=xxxxxxx
NATADECOCO_ID=XXXXXXX
NATADECOCO_SECRET=XXXXXXX
NODE_ENV=stg
EXEC_ENV=local
APPLAUSE_URL=http://localhost:8080
AUDIENCE_SIZE=500
DECIMATION_RATE=0.2
TEST_AUDIENCE_SIZE=100
```

### 2. 初期設定(インストール)

```
npm install
npm run datastore:setup
npm run datastore:seed
```

### 3. サーバーアプリを立ち上げる

```
npm start
```

`localhost:3030` に立ち上がります。

## Serviceの追加方法

### feathers-cliをインストール

```
npm install -g feathers-cli
```

### Serviceを追加

```
feathers generate service
```

基本的には `A custom service` を選択する。
