# COCO Cheer

![COCO Cheer](./app/client/src/assets/image/coco-cheer-logo_tomato.png)

natadeCOCO Cheer App

「心地良いや」と思える空間を作り出す、歓声アプリ

## Dependencies

- [Applause Sound Server](https://gitlab.com/natade-coco/extras/applause) (0.3.0)
    - 先に起動している必要があります

## Environments

| key | type | default | 説明 |
| :-- | :-- | :-- | :-- |
| `APPLAUSE_URL` | string | http://applause.default.svc.cluster.local:8080 | ApplauseのURL |
| `AUDIENCE_SIZE` | number | 500 | 想定観客数 |
| `DECIMATION_RATE` | number | 0.2 | 再生する歓声を間引くための係数 |
| `TEST_AUDIENCE_SIZE` | number | 0 | テスト用の観客数 |

### 想定観客数について

想定観客数は、フリフリ応援の間引きおよび盛り上がりレベルの算定に使用されます。

フリフリ応援の間引きは、下記の計算式によって求められる間隔ごとに1回のみ音声再生するというロジックにより行われます。

- 間隔(ms) = `AUDIENCE_SIZE / 1500 / DECIMATION_RATE`
- `AUDIENCE_SIZE * DECIMATION_RATE` の値が100前後を推奨

盛り上がりレベルは、1500msごとに、 `フリフリ応援の数 / AUDIENCE_SIZE` で計算される割合と下記表によって算出されます。

| 割合 | 盛り上がりレベル | 説明 |
| :-- | :-- | :-- |
| <= 0 | zero | 盛り上がりなし |
| < 0.2 | low | 小盛り上がり |
| < 0.5 | mid | 中盛り上がり |
| >= 0.5 | high | 大盛り上がり |

## Datastore

| collection | 説明 |
| :-- | :-- |
| `cheer_sounds` | 音声ファイルに対応するアイテム `Sound` |
| `cheer_categories` | 1タブに対応するアイテム `Category` |
| `cheer_sound_groups` | 1ボタンに対応するアイテム `SoundGroup` |

### 音声設定手順(概要)

1. ファイルライブラリーにWAVファイル(推奨: 16bit 22050Hz mono)を追加
1. `cheer_sounds` コレクションに `Sound` アイテムを作成(↑で追加したファイルと紐付ける)
1. `cheer_categories` コレクションに `Category` アイテムを作成
1. `cheer_sound_groups` コレクションに `SoundGroup` アイテムを作成(複数の `Sound` と紐付ける)

### 特記事項

- 1つの `SoundGroup` に複数の `Sound` が紐付いているとき、ランダムで再生される
- コレクションに何らかの変更があった場合(ソートを除く)、サーバーアプリがロードし直される
- サーバーアプリはロード時、Directusからデータを取得後、メモリおよびApplauseにデータの登録を実施する
