# Festival Live

2023年2月22日

## 設定値

### 環境変数

- AUDIENCE_SIZE=500 (デフォルト)
- DECIMATION_RATE=0.2 (デフォルト)

### SoundのVolume

- Applause1〜3: -2dB
- その他: 0dB

## Audacityによる音声編集手順メモ

1. 余分な部分をカット
1. ラウドネスノーマライズ => 知覚的ラウドネス -15.0 LUFS
1. プロジェクトのサンプリング周波数(Hz) => 22050 Hz
1. Shift + Cmd + L で書き出し => ファイル名プレフィックスの後に番号付け
